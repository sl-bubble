key avatar;
integer perms;
vector velocity;

float MAX_VELOCITY = 20.0;
float EPSILON = 0.01;

reset()
{
    llSetRot(llEuler2Rot(<0, 0, 0>));
    llSetPrimitiveParams([PRIM_MATERIAL, PRIM_MATERIAL_GLASS]);
    llSitTarget(<.01, 0, 0>, llEuler2Rot(<0, -25 * DEG_TO_RAD, 0>));
    llSetStatus(STATUS_PHYSICS, FALSE);
    perms = 0;
    velocity = ZERO_VECTOR;
}

changed_sit(key av)
{
    avatar = av;
    reset();
    if (avatar) {
        llRequestPermissions(avatar, PERMISSION_TAKE_CONTROLS | PERMISSION_TRIGGER_ANIMATION);
    }
}

default {
    state_entry() {
        reset();
    }

    on_rez(integer param) {
        llResetScript();
    }

    changed(integer what) {
        if (what & CHANGED_LINK) {
            changed_sit(llAvatarOnSitTarget());
        }
    }

    run_time_permissions(integer permissions) {
        perms = perms | permissions;
        if (perms & PERMISSION_TAKE_CONTROLS && perms & PERMISSION_TRIGGER_ANIMATION) {
            state flying;
        }
    }
}

state flying {
    state_entry() {
        llTakeControls(
                CONTROL_FWD |
                CONTROL_BACK |
                CONTROL_LEFT |
                CONTROL_RIGHT |
                CONTROL_UP |
                CONTROL_DOWN |
                CONTROL_ROT_LEFT |
                CONTROL_ROT_RIGHT |
                0, TRUE, FALSE);
        llStopAnimation("sit");
        llStartAnimation("fly");
        llSetVehicleType(VEHICLE_TYPE_BALLOON);
        llSetVehicleFloatParam(VEHICLE_HOVER_HEIGHT, 0);
        llSetVehicleFloatParam(VEHICLE_LINEAR_MOTOR_TIMESCALE, 0.1);
        llSetVehicleFloatParam(VEHICLE_LINEAR_MOTOR_DECAY_TIMESCALE, 0.2);
        llSetVehicleFloatParam(VEHICLE_LINEAR_FRICTION_TIMESCALE, 0.2);
        llSetVehicleFloatParam(VEHICLE_ANGULAR_MOTOR_TIMESCALE, 0.1);
        llSetVehicleFloatParam(VEHICLE_ANGULAR_MOTOR_DECAY_TIMESCALE, 0.1);
        llSetVehicleFloatParam(VEHICLE_ANGULAR_FRICTION_TIMESCALE, 0.1);
        llSetVehicleFlags(VEHICLE_FLAG_CAMERA_DECOUPLED);
        llSetStatus(STATUS_PHYSICS, TRUE);
        velocity = <0, 0, 0>;
    }

    state_exit() {
        llSetVehicleType(VEHICLE_TYPE_NONE);
        llSetStatus(STATUS_PHYSICS, FALSE);
        llReleaseControls();
        llStopAnimation("fly");
    }

    control(key name, integer levels, integer edges) {
        vector real_velocity = llGetVel();
        if (llFabs(real_velocity.z) < EPSILON)
            velocity.z = 0;
        if (llFabs(real_velocity.x) < EPSILON)
            velocity.x = 0;
        if (llFabs(real_velocity.y) < EPSILON)
            velocity.y = 0;
        if (levels & CONTROL_UP) {
            if (velocity.z < 0)
                velocity.z = 0;
            else
                velocity += <0, 0, 1>;
            velocity.x = velocity.y = 0;
        } else if (levels & CONTROL_DOWN) {
            if (velocity.z > 0)
                velocity.z = 0;
            else
                velocity -= <0, 0, 1>;
            velocity.x = velocity.y = 0;
        } else if (levels & CONTROL_BACK) {
            if (velocity.x > 0)
                velocity.x = 0;
            else
                velocity -= <1, 0, 0>;
            velocity.y = velocity.z = 0;
        } else if (levels & CONTROL_FWD) {
            if (velocity.x < 0)
                velocity.x = 0;
            else
                velocity += <1, 0, 0>;
            velocity.y = velocity.z = 0;
        } else if (levels & CONTROL_LEFT) {
            if (velocity.y < 0)
                velocity.y = 0;
            else
                velocity += <0, 1, 0>;
            velocity.x = velocity.z = 0;
        } else if (levels & CONTROL_RIGHT) {
            if (velocity.y > 0)
                velocity.y = 0;
            else
                velocity -= <0, 1, 0>;
            velocity.x = velocity.z = 0;
        } else if (levels & CONTROL_ROT_LEFT) {
            llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <0, 0, PI>);
        } else if (levels & CONTROL_ROT_RIGHT) {
            llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <0, 0, -PI>);
        }
        float velocity_magnitude = llVecMag(velocity);
        if (velocity_magnitude > MAX_VELOCITY)
            velocity *= MAX_VELOCITY / velocity_magnitude;
        llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION, velocity);
    }

    changed(integer what) {
        if (what & CHANGED_LINK) {
            state default;
            changed_sit(llAvatarOnSitTarget());
        }
    }
}
